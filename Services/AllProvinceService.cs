﻿using BBU_SCHEDULE_API.Configuration;
using BBU_SCHEDULE_API.Interfaces;
using BBU_SCHEDULE_API.Models;
using System.Reflection;

namespace BBU_SCHEDULE_API.Services
{
    public class AllProvinceService:IAllProvince
    {
        private readonly IConfiguration configuration; 
        private AppDBContext context;
        double pageResult = 10;



        public AllProvinceService(AppDBContext context, IConfiguration configuration) 
        {
            this.configuration = configuration;
            this.context = context;
        }

        public double Count()
        {
            return context.allProvinces!.Count();
        }

        public double Count(int id)
        {
            return context.allProvinces!.Where(s => s.PorviceID == id).Count();
        }

        public double PageCount()
        {
            return Math.Ceiling((double)context.allProvinces!.Count() / pageResult)!;
        }
        public double PageCount(int id)
        {
            return Math.Ceiling((double)context.allProvinces!.Where(b => b.PorviceID == id).Count() / pageResult)!;
        }

        public List<AllProvince> GetProvinces() 
        {
            var banners = context.allProvinces!
                    .OrderByDescending(d => d.PorviceID)
                    .ToList();
            if (banners != null)
            {
                return banners!;
            }
            return null!;
        }

        public AllProvince GetProvinceById(int id) 
        {
            AllProvince allProvince = context.allProvinces!.Where(l => l.PorviceID == id).SingleOrDefault()!;
            if (allProvince != null)
            {
                return allProvince;
            }
            return null!;
        }

        public AllProvince CreateNew(AllProvince req)
        {
           
            context.Add(req);
            context.SaveChanges();
            AllProvince result = context.allProvinces!.Where(u => u.PorviceID == req.PorviceID).FirstOrDefault()!;
            return result;

        }

        public AllProvince Update(AllProvince req)
        {
            AllProvince banner = context.allProvinces!.FirstOrDefault(c => c.PorviceID == req.PorviceID)!;
            context.SaveChanges();
            AllProvince result = context.allProvinces!.Where(u => u.PorviceID == req.PorviceID).FirstOrDefault()!;
            if (result != null)
            {
                return result;
            }
            return null!;
        }
        public AllProvince Delete(int id)
        {
            var result = context.allProvinces!.FirstOrDefault(s => s.PorviceID == id);
            if (result != null)
            {
                context.Remove(id);
            }
            
            return null!;

        }
    }
}
