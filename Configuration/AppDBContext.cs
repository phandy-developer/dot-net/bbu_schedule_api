﻿using BBU_SCHEDULE_API.Models;
using Microsoft.EntityFrameworkCore;

namespace BBU_SCHEDULE_API.Configuration
{
    public class AppDBContext : DbContext
    {
        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options) { }

        public DbSet<AllProvince>? allProvinces { get; set; }
    }
}
