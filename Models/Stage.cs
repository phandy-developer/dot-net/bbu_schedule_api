﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[stages]")]
    public class Stage
    {
        [Key]
        public int id { get; set; }
        public string? stagename { get; set; }
    }
}

