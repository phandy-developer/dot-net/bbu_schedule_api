﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_labs]")]
    public class Lab
    {
        [Key]
        public int LabID { get; set; }
        public int UserID { get; set; }
        public string? LabName { get; set; }
        public string? LabNameInKhmer { get; set; }
        public int Capactity { get; set; }
        public string? Description { get; set; }
        public string? LastDateUpdate { get; set; }
    }
}
