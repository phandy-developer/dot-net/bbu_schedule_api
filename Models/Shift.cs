﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[shifts]")]
    public class Shift
    {
        [Key]
        public int ShiftID { get; set; }
        public string? ShiftName { get; set; }
        public string? ShiftDescription { get; set; }
    }
}
