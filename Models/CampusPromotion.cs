﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_promotion]")]
    public class CampusPromotion
    {
        [Key]
        public int PromotionID { get; set; }
        public int Promotion { get; set; }
        public int Start { get; set; }
        public int End { get; set; }
        public string? Description { get; set; }
    }
}
