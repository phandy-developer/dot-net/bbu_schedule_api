﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_teacher_position]")]
    public class TeacherPositionTitle
    {
        [Key]
        public int id { get; set; }
        public string? namekh { get; set; }
        public string? nameeng { get; set; }
    }
}
