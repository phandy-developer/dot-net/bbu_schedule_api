﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_daytime]")]
    public class DayTime
    {
        [Key]
        public int DTID { get; set; }
        public int DayID { get; set; }
        public int TimeID { get; set; }
    }
}
