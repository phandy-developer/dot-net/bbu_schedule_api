﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[normcatg]")]
    public class NormCategory
    {
        [Key]
        public int id { get; set; }
        public string? normcatgname { get; set; }
        public int status { get; set; }
        public string? description { get; set; }
    }
}
