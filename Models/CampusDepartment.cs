﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("tbl_departments")]
    public class CampusDepartment
    {
        [Key]
        public int DepartID { get; set; }
        public int FacultyID { get; set; }
        public string? DepartName { get; set; }
        public string? DepartNameinKhmer { get; set; }
        public string? ShortName { get; set; }
        public int NumberID { get; set; }
    }
}
