﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_faculty]")]
    public class Faculty
    {
        [Key]
        public int FacultyID { get; set; }
        public int CampusID { get; set; }
        public string? FacultyName { get; set; }
        public string? FacultyinKhmer { get; set; }
        public string? ShortName { get; set; }
        public string? FShortName { get; set; }
    }
}
