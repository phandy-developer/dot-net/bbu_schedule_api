﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[normexception]")]
    public class NormException
    {
        [Key]
        public int id { get; set; }
        public int TeaID { get; set; }
        public int FacultyID { get; set; }
        public string? Descriotions { get; set; }
        public int statusex { get; set; }
    }
}
