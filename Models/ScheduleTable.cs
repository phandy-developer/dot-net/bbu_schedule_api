﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_scheduletable]")]
    public class ScheduleTable
    {
        [Key]
        public int ScheduleDetailID { get; set; }
        public int joinID { get; set; }
        public int TeaID { get; set; }
        public int SubjID { get; set; }
        public int RoomID { get; set; }
        public int LabID { get; set; }
        public int DTID { get; set; }
        public string? GoupNote { get; set; }
        public int TBStatus { get; set; }
    }
}
