﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_scheduledetail]")]
    public class ScheduleJoin
    {
        [Key]
        public int ScheduleDetailID { get; set; }
        public int ScheduleID { get; set; }
        public int UserID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime MidDate { get; set; }
        public DateTime toMidDate { get; set; }
        public DateTime ExamDate { get; set; }
        public DateTime NewTerDate { get; set; }
        public int ScheduleType { get; set; }
        public int Status { get; set; }
        public string? Note { get; set; }
        public string? StartDateChanged { get; set; }
        public DateTime ReserveDate { get; set; }
        public DateTime ChangedDate { get; set; }
        public int changID { get; set; }
        public int StatusSubmit { get; set; }
    }
}
