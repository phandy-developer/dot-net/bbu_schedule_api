﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_schedulelist]")]
    public class Schedule
    {
        [Key]
        public int ScheduleID { get; set; }
        public int PromotionID { get; set; }
        public string? Stage { get; set; }
        public string? Term { get; set; }
        public int MGID { get; set; }
        public int DegreeID { get; set; }
        public int CFID { get; set; }
        public int FieldID { get; set; }
        public int ScheduleType { get; set; }
    }
}
