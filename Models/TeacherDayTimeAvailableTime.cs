﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_teacher_aviable_time]")]
    public class TeacherDayTimeAvailableTime
    {
        [Key]
        public int TATID { get; set; }
        public int TeaID { get; set; }
        public int DTID { get; set; }
        public int Status { get; set; }
        public string? Description { get; set; }
    }
}
