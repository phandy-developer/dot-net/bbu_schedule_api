﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_holidays]")]
    public class HolidayList
    {
        [Key]
        public int HOLIDAYID { get; set; }
        public string? HOLIDAYNAME { get; set; }
        public string? HOLIDAYYEAR { get; set; }
        public string? HOLIDAYMONTH { get; set; }
        public int HOLIDAYDAY { get; set; }
        public DateTime STARTTIME { get; set; }
        public int DURATION { get; set; }
        public string? HOLIDAYTYPE { get; set; }
        public string? XINBIE { get; set; }
        public string? MINZU { get; set; }
        public int DeptID { get; set; }
    }
}
