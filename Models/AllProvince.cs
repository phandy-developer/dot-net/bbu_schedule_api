﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("tbl_province")]
    public class AllProvince
    {
        [Key]
        public int PorviceID { get; set; }
        public string? Province { get; set; }
        public string? ProviceNameinKhmer { get; set; }
    }
}
