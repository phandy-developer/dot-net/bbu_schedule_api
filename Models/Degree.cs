﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_degree]")]
    public class Degree
    {
        [Key]
        public int DegreeID { get; set; }
        public string? DegreeName { get; set; }
        public string? DegreeNameinKhmer { get; set; }
        public string? Note { get; set; }
    }
}
