﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_subjectbyterm]")]
    public class SubjectPromotionByTermStudy
    {
        [Key]
        public int pstsubjID { get; set; }
        public int CFID { get; set; }
        public int Promotion { get; set; }
        public int Stage { get; set; }
        public int Term { get; set; }
        public int FieldID { get; set; }
        public int subcodeuseID { get; set; }
        public decimal Credit { get; set; }
        public decimal CreditHour { get; set; }
        public string? Note { get; set; }
        public int Status { get; set; }
    }
}
