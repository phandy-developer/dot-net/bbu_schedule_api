﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_room_aviable_time]")]
    public class RoomDayTimeAvailableTime
    {
        [Key]
        public int RATID { get; set; }
        public int RoomID { get; set; }
        public int DTID { get; set; }
        public int Status { get; set; }
        public string? Description { get; set; }
    }
}
