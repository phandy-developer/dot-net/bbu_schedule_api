﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[exchangecast]")]
    public class ExchangeCurrency
    {
        [Key]
        public int ExCaId { get; set; }
        public int CastCatId { get; set; }
        public decimal CastValueReil { get; set; }
        public DateTime ExCaDate { get; set; }
        public string? ExCaDescription { get; set; }
    }
}
