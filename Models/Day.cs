﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_days]")]
    public class Day
    {
        [Key]
        public int day_id { get; set; }
        public string? day_name { get; set; }
    }
}
