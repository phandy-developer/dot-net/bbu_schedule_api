﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_currencytype]")]
    public class Currency
    {
        [Key]
        public int curcid { get; set; }
        public string? Curcname { get; set; }
        public string? signcurc { get; set; }
        public string? curc_description { get; set; }
    }
}
