﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_group]")]
    public class CampusGroup
    {
        [Key]
        public int GroupID { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }
        public int statusg { get; set; }
        public int orderb { get; set; }
    }
}
