﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[countries]")]
    public class Country
    {
        [Key]
        public int id { get; set; }
        public string? name { get; set; }
        public string? nameeng { get; set; }
        public string? alpha_2 { get; set; }
        public string? alpha_3 { get; set; }
    }
}
