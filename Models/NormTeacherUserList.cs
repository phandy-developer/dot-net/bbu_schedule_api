﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[normteacherlist]")]
    public class NormTeacherUserList
    {
        [Key]
        public int id { get; set; }
        public int TeachID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int normcatg_id { get; set; }
        public decimal norminHours { get; set; }
        public int statusNorm { get; set; }
    }
}
