﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[tbl_rooms]")]
    public class CampusRoom
    {
        [Key]
        public int RoomID { get; set; }
        public string? RoomName { get; set; }
        public string? RoomNameKh { get; set; }
        public int Capacity { get; set; }
        public string? Description { get; set; }
        public string? LastUpdate { get; set; }
        public int statusr { get; set; }
        public string? orderby { get; set; }
        public int build_id { get; set; }
    }
}
