﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[normorderbymonthly]")]
    public class NormTeacherUserListMonthly
    {
        [Key]
        public int id { get; set; }
        public DateTime dateEnd { get; set; }
        public decimal NormHoursDebt { get; set; }
        public int StatUs { get; set; }
        public int TeaID { get; set; }
        public decimal ActualTeach { get; set; }
        public decimal normHours { get; set; }
        public DateTime monthlyperyear { get; set; }
        public decimal NormThisMonth { get; set; }
        public DateTime paydate { get; set; }
        public int invid { get; set; }
        public int catg_id { get; set; }
        public int BudCPID { get; set; }
        public decimal variance_subtract { get; set; }
    }
}
