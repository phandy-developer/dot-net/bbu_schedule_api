﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("tbl_users")]
    public class ApplicationUser
    {
        [Key]
        public int UserID { get; set; }
        public int CFID { get; set; }
        public string? UserName { get; set; }
        public string? FullName { get; set; }
        public string? HeadName { get; set; }
        public string? Password { get; set; }
        public int CountLogin { get; set; }
        public int AccoutType { get; set; }
        public string? GroupUser { get; set; }
        public string? LastDateLogin { get; set; }
        public string? Tel { get; set; }
        public string? Title1 { get; set; }
        public string? Title2 { get; set; }
        public string? Note { get; set; }
        public string? TiltleH1 { get; set; }
        public string? TitleH2 { get; set; }
        public int Status { get; set; }
    }
}
