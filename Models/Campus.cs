﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("tbl_campus")]
    public class Campus
    {
        [Key]
        public int CampusID { get; set; }
        public string? CampusName { get; set; }
        public string? CampusInKhmer { get; set; }
        public string? ShortName { get; set; }
        public string? Address { get; set; }
        public string? Fax { get; set; }
        public string? Tell { get; set; }
    }
}
