﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("[dbo].[htbl_rateteacher]")]
    public class TeacherRate
    {
        [Key]
        public int HRTID { get; set; }
        public int RTID { get; set; }
        public int RTSID { get; set; }
        public int CampusID { get; set; }
        public decimal Rate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Status { get; set; }
        public string? NoteEng { get; set; }
        public string? NoteKh { get; set; }
        public string? Other { get; set; }
        public int ratcatid { get; set; }
        public int degreeid { get; set; }
        public DateTime recordDate { get; set; }
        public int usid { get; set; }
        public DateTime datedelete { get; set; }
        public int userdelete { get; set; }
    }
}
