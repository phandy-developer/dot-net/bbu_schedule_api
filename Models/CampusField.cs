﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BBU_SCHEDULE_API.Models
{
    [Table("tbl_fields")]
    public class CampusField
    {
        [Key]
        public int FieldID { get; set; }
        public int DepartID { get; set; }
        public int DegreeID { get; set; }
        public string? Field { get; set; }
        public string? FieldNameinKhmer { get; set; }
        public string? ShortName { get; set; }
        public int NumberID { get; set; }
        public int statusf { get; set; }
    }
}
