﻿using BBU_SCHEDULE_API.Interfaces;
using BBU_SCHEDULE_API.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BBU_SCHEDULE_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AllProvinceController : ControllerBase
    {
        private IAllProvince service;
        public AllProvinceController(IAllProvince service)
        {
            this.service = service;
        }
        // GET: api/<AllProvinceController>
        [HttpGet]
        public IActionResult Get()
        {
            List<AllProvince> result = service.GetProvinces();
            return Ok(new
            {
                code = "200",
                message = "Successfully!",
                count = service.Count(),
                countPage = service.PageCount(),
                currentPage = 1,
                data = result
            });
        }

        // GET api/<AllProvinceController>/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            //​​var result = service.GetProvinceById(id);
            return Ok(new
            {
                code = "200",
                message = "Successfully!",
                count = service.Count(),
                countPage = service.PageCount(),
                currentPage = 1,
                //data = result
            });
        }

        // POST api/<AllProvinceController>
        [HttpPost]
        public IActionResult Post([FromBody] AllProvince req)
        {
            service.CreateNew(req);
            return Ok(new
            {
                code = "200",
                message = "Successfully!",
                count = service.Count(),
                countPage = service.PageCount(),
                currentPage = 1,
                data = service.GetProvinceById(req.PorviceID)
            }) ;
        }

        // PUT api/<AllProvinceController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] AllProvince req)
        {
            service.Update(req);
            return Ok(new
            {
                code = "200",
                message = "Successfully!",
                count = service.Count(),
                countPage = service.PageCount(),
                currentPage = 1,
                data = service.GetProvinceById(req.PorviceID)
            });
        }

        // DELETE api/<AllProvinceController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            service.Delete(id);
            return Ok(new
            {
                code = "200",
                message = "",
                count = service.Count(),
                countPage = service.PageCount(),
                currentPage = 1,
                data = service.GetProvinceById(id)
            });
        }
    }
}
