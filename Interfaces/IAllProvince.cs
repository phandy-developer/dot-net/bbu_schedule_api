﻿using BBU_SCHEDULE_API.Models;

namespace BBU_SCHEDULE_API.Interfaces
{
    public interface IAllProvince
    {
        public List<AllProvince> GetProvinces(); 
        public AllProvince GetProvinceById(int id);


        public double Count();
        public double PageCount();
        public double Count(int id);
        public double PageCount(int id);



        public AllProvince Update(AllProvince obj);
        public AllProvince CreateNew(AllProvince req);
        public AllProvince Delete(int id);
    }
}
